//api.trace("quote","",quote)
def totalInvoicePrice = api.getElement("TotalInvoicePrice")

def approverLevel = api.findLookupTableValues("Approval").collectEntries {
  [(it.name): [it.attribute1,  it.attribute2, it.attribute3]]
}

approverLevel.each { level ->
  	
  if(level.value[0]) {
  	workflow.addApprovalStep("${level.key}")
  
	def reasons = [:]
    def text = []

	if (level.value[0] && level.value[1] && (totalInvoicePrice >= level.value[1])) {
  	  
      text << "Total Invoice Price is ${level.value[1]}€ or higher"
      reasons << ["${level.key}" : text]
    
  	  workflow.getStep("${level.key}")
  		  		.withUserGroupApprovers("${level.value[0]}")
  		  		.setReason("Total Invoice Price is ${level.value[1]}€ or higher")
    } 

	def marginPerc = 0.0

	quote.lineItems.each { line ->

		if (line.folder) { return }
  		marginPerc = line.outputs?.find { it.resultName == "MarginPerc" }?.result
  api.trace("marginPerc","",marginPerc)
  		if (level.value[0] && marginPerc < level.value[2] && marginPerc != null) {
    		
          text << "${line.sku} margin % is lower then ${level.value[2] * 100}%"
		  reasons << ["${level.key}" : text.join(", \n")]

    	  workflow.getStep("${level.key}")
          			.withUserGroupApprovers("${level.value[0]}")
          			.setReason("${reasons.values().join()}")
  		}
	}
  }
  	workflow.steps.each { step ->
  		if (step.reason == null) {
    	workflow.removeStep("${step.uniqueName}")
  		}
  	}
}