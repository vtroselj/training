workflow
    .addApprovalStep("Sales Manager")
    .withUserGroupApprovers("Sales Managers")
    .setReason("Sales Manager must always approve!")