api.logInfo("---DEBUG: Start run VolumeTier-CFS", null)

//Get CFS
def cfs = api.findCalculatedFieldSets(api.targetDate(), "VolumeTier")[0]
api.trace("CFS-status","",cfs.status)

//Run CFS if it's ready (draft or error)
if (["DRAFT", "ERROR", "READY"].contains(cfs.status)) {
  	actionBuilder
	.addCalculatedFieldSetAction('VolumeTier')
		.setCalculate(true)
	api.global.cfs = "STARTED"
  	api.logInfo("---DEBUG: Start CFS", null)
} else if (["READY"].contains(cfs.status)) {
  	api.global.cfs = "READY"
}

api.logInfo("---DEBUG: Running CFS calculation. api.global.cfs is ", api.global.cfs)