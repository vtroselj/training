if(api.global.lastRun != null) return

api.logInfo("setting global.lastRun because it is ", api.global.lastRun)

def lastRunString = api.vLookup("Timestamp", "VolumeTierTimestamp")
api.logInfo("---DEBUG: lastRunString is ", lastRunString)
api.trace("lastRunString", "", lastRunString)

// transform lastRun from string to dateTime
def lastRunDateTime = (lastRunString != null && !"NONE".equals(lastRunString)) ? api.datamartCalendar().parseDateTime(lastRunString) : null
api.logInfo("lastRunDateTime is ", lastRunDateTime)
api.trace("lastRunDateTime","",lastRunDateTime)

// Set global variables so this don't have to run for each row
api.global.lastRun = lastRunDateTime
api.global.pptimestamp = lastRunDateTime
api.trace("Test","",api.global)