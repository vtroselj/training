if(api.isSyntaxCheck()) return

//def date = api.targetDate()?.format("yyyy-MM-dd") + 'T' + (new Date()).format("HH:mm:ss")
//api.trace("date","",date)



// Get last run from global variable
def lastRun = api.global.lastRun
api.trace("lastRun","",lastRun)
def date = api.targetDate()?.format("yyyy-MM-dd") + 'T' + (new Date()).format("HH:mm:ss")
api.trace("Date","",date.class)

def currentProductUpdateDateTime = (api.productExtension("VolumeTier")) ? api.datamartCalendar().parseDateTime(api.productExtension("VolumeTier").find().lastUpdateDate) : api.datamartCalendar().parseDateTime(date)
api.trace("DateTime","",currentProductUpdateDateTime)



def discountBeef = api.findLookupTableValues("VT_Beef")
def discountLamb = api.findLookupTableValues("VT_Lamb")
api.trace("Beef","",discountBeef)
api.trace("Lamb","",discountLamb)

// Check if current item is updated after lastRun, if so add it to Price Parameter
if (lastRun == null || currentProductUpdateDateTime.isAfter(lastRun)) {
  	
  	//Add product to Price Parameter
	if (api.product("attribute1") == "Beef") {
  		discountBeef.each {
        api.addOrUpdate("PX", ["name": "VolumeTier",
                               "sku": api.product("sku"),
                               "attribute1": api.product("attribute1"),
                               "attribute2": it.name,
                               "attribute3": it.value])
    }
  } else if (api.product("attribute1") == "Lamb") {
      discountLamb.each {
          api.addOrUpdate("PX", ["name": "VolumeTier",
                                  "sku": api.product("sku"),
                                  "attribute1": api.product("attribute1"),
                                  "attribute2": it.name,
                                  "attribute3": it.value])
    }
  }
  
  	//Update pricing parameter table timestamp - if current item has updated date later than last updated
  	def pptimestamp = api.global.pptimestamp
  	if (pptimestamp == null || currentProductUpdateDateTime.isAfter(pptimestamp)) {
    	api.global.pptimestamp = currentProductUpdateDateTime
      	api.addOrUpdate("LTV", ["lookupTableName": "Timestamp",
                               	"name": "VolumeTierTimestamp",
                               	"value": currentProductUpdateDateTime])
  }
}