if(api.isSyntaxCheck()) return

api.global.sideDishPrices = []
api.trace("","",api.global.sideDishes)
api.global.sideDishes.each {
  //api.trace("","",it)
  api.global.sideDishPrices += "€" + api.vLookup("FoodPrices", "List Price per 100g", it)
  
}
api.trace("","",api.global.sideDishPrices)

return api.global.sideDishPrices.join(", \n")
