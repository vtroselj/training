if(api.isSyntaxCheck()) return

def meatPrice = api.vLookup("FoodPrices", "List Price per 100g", api.global.configurator.Meat)
api.trace("","",meatPrice)
return meatPrice