def totalSideDishPrice = 0
//return api.global.sideDishes[0]
api.global.sideDishesWeight.eachWithIndex { it, i ->
  //api.trace("it","",it)
 def sideDishWeight = it.split("g")[0].toInteger()
 def sideDishPrice = api.vLookup("FoodPrices", "List Price per 100g", api.global.sideDishes[i]) 
 //api.trace("","",sideDishPrice)
 totalSideDishPrice += sideDishPrice * (sideDishWeight/100)
}
return totalSideDishPrice