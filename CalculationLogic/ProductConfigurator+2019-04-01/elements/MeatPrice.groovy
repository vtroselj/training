if(api.isSyntaxCheck()) return
def meatWeight = api.getElement("MeatWeight").split("g")[0].toInteger()
def meatPrice = api.getElement("MeatPrice100g") * (meatWeight/100)
return meatPrice
