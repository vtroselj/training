def price = api.productExtension("Costs")?.find()?.attribute1
def relatedProductPrice

if(price != null) {
  
  return price

} else {
  
  if(api.getIterationNumber() == 0) {
    api.markItemDirty()
  
  } else {
    
    relatedProductPrice = api.currentContext("Gas-0001")?.ResultPrice
    price = relatedProductPrice + (relatedProductPrice * api.vLookup("AnchorPricing", "Price", api.product("label")))
  }
}

return price