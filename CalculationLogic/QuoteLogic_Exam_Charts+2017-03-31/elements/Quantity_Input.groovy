def quantity = api.integerUserEntry("Quantity")

def p = api.getParameter("Quantity")
if( p != null && p.getValue() == null) {
  p.setRequired(true)
}
return quantity