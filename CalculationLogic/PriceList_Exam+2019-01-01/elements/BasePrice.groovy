def BasePrice = api.productExtension("Costs")?.find()?.attribute1
def anchorPrice = api.findLookupTableValues("AnchorPricing", Filter.equal("name", api.product("label")))?.find()?.attribute1
def resultPrice1 = 0

if(BasePrice) {
  
  return BasePrice

}

if (anchorPrice && api.getIterationNumber() == 0) {
  
  		api.markItemDirty()
  
	} 

if(anchorPrice != null) {
    
        def relatedProductPrice = api.currentContext("Gas-0001")?.ResultPrice
  		
  		if(relatedProductPrice) {
        
          resultPrice1 = relatedProductPrice + (relatedProductPrice * api.vLookup("AnchorPricing", "Price", api.product("label")))
      
        return resultPrice1
          
        } else {
          api.addWarning("Missing product cost!")
  		  api.abortCalculation()
        }
} else  { 
      
    	api.addWarning("Missing product cost!")
  		api.abortCalculation()      
  	  }