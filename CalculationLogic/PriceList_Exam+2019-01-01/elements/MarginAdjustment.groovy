def marginAdjPerc = (api.getElement("ProductGroup")) ? api.findLookupTableValues("MarginAdjustment", Filter.equal("name", api.getElement("ProductGroup")))?.find()?.attribute1 : null
api.trace("","",marginAdjPerc)
if (!marginAdjPerc) {
    api.addWarning("Missing margin adjustment!")
  	api.abortCalculation()
} else {
    def marginAdjustment = api.getElement("BasePrice") * marginAdjPerc / ( 1 - marginAdjPerc)
    return marginAdjustment
  }