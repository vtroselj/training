def attributeAdjPerc = (api.getElement("ProductLifeCycle")) ? api.findLookupTableValues("AttributeAdjustment", Filter.equal("name", api.getElement("ProductLifeCycle")))?.find()?.attribute1 : null
api.trace("","",attributeAdjPerc)
if(!attributeAdjPerc) {
  api.addWarning("Missing attribute adjustment!")
  api.abortCalculation()
} else{
  def attributeAdjustment = (api.getElement("BasePrice") + api.getElement("MarginAdjustment")) * attributeAdjPerc / (1 - attributeAdjPerc)
}