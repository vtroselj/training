if (quoteProcessor.isPostPhase()) {
    getFolderSummary("ROOT")
}
def getFolderSummary(folderId) {
    def h = quoteProcessor.getHelper()
    def folderSummary = [
            totalListPrice   : 0.0,
            totalMargin      : 0.0
    ]

    def folder = (folderId == "ROOT") ? h.getRoot() : h.findByLineId(folderId)
    def lineTotal

    for (line in folder.getChildren()) {


        if (line.isFolder()) {
            lineTotal = getFolderSummary(line.getLineId())
        } else {

            def margin = line.getOutputByName("Margin")?.result ?: 0
            def quantity = line.getOutputByName("Quantity")?.result ?: 0
            def listPrice = (!margin) ? 0 : line.getOutputByName("ListPrice")?.result

            if (!margin) {
                api.addWarning("${line.SKU} won't be calculated in Overall Margin % because Margin is missing!")
            }

            lineTotal = [
                    totalListPrice: quantity * listPrice,
                    totalMargin: quantity * margin
            ]
        }

        folderSummary.totalMargin = folderSummary.totalMargin + lineTotal.totalMargin
        folderSummary.totalListPrice = folderSummary.totalListPrice + lineTotal.totalListPrice


    }

    def totalNetMarginPercentage = folderSummary.totalListPrice ? (folderSummary.totalMargin / folderSummary.totalListPrice) : 0

    if (!totalNetMarginPercentage) {
        quoteProcessor.addOrUpdateOutput("ROOT",
                [
                        "resultName" : "MarginPerc",
                        "resultLabel": "Margin %",
                        "formatType" : "PERCENT",
                        "result"     : totalNetMarginPercentage,
                ])

        quoteProcessor.addOrUpdateOutput(folderId,
                [
                        "resultName" : "MarginPerc",
                        "resultLabel": "Margin %",
                        "formatType" : "PERCENT",
                        "result"     : totalNetMarginPercentage,
                ])
    }   else {
        quoteProcessor.addOrUpdateOutput(folderId,
                [
                        "resultName" : "MarginPerc",
                        "resultLabel": "Margin %",
                        "formatType" : "PERCENT",
                        "result"     : totalNetMarginPercentage,
                ])
        }


        if (folder == null) return folderSummary

        return folderSummary
}