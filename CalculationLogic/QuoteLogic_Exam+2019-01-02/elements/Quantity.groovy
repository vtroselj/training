def quantity = api.integerUserEntry("Quantity")
def p = api.getParameter("Quantity")

if (p != null && p.getValue() == null) {
    p.setRequired(true)
}

if (quantity == null){
    api.addWarning("Enter Quantity!!!")
    api.criticalAlert("Missing Quantity!!!")
    api.abortCalculation()


} else if (quantity <= 0) {
    api.criticalAlert("You are not selling anything!!!")
    api.abortCalculation()
    return quantity


} else {
    return quantity
}




