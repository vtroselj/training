if (api.getElement("Margin") == null ){
    api.redAlert("Missing Margin!!!")
    return null
} else if (api.getElement("ListPrice") == null){
    api.redAlert("Missing List Price!!!")
    return null
} else {

    def marginPerc = api.getElement("Margin") / api.getElement("ListPrice")
    def approverLevel = api.findLookupTableValues("Approval").collectEntries {
        [(it.name): [it.attribute1, it.attribute2, it.attribute3]]
    }
    def level1
    def level2

    for (level in approverLevel) {
        if (level.key == "Level 1") {
            level1 = level.value[2]
        }
        if (level.key == "Level 2") {
            level2 = level.value[2]
        }

        if ((marginPerc < level1 && marginPerc >= level2) && level.key == "Level 1"){
            api.yellowAlert("${level.value[0]} must approve document!")
        } else if ((marginPerc < level2) && level.key == "Level 2") {
            api.redAlert("${level.value[0]} must approve document!")
        }
    }

    return marginPerc

}
