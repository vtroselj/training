def margin

if (api.getElement("InvoicePrice") == null){
    api.redAlert("Missing Invoice Price!!!")
    return null
} else if (api.getElement("Costs") == null) {
    api.redAlert("Missing Cost!!!")
    return null
} else {
    margin = api.getElement("InvoicePrice") - api.getElement("Costs")
    return margin
}




