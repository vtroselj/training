def salesDiscount1

if (api.getElement("SalesDiscountPercent") < 0){
    api.redAlert("Missing Sales Discount %!!!")
    return  null
} else if (api.getElement("ListPrice") == null){
    api.redAlert("Missing List Price!!!")
    return null
} else {
    salesDiscount1 = api.getElement("SalesDiscountPercent") * api.getElement("ListPrice")
    return salesDiscount1
}





