def salesDiscount = api.integerUserEntry("Sales Discount %")

def p = api.getParameter("Sales Discount %")

if (salesDiscount == null) {
    api.criticalAlert("Please enter Sales Discount %!!!")
    api.addWarning("Enter Sales Discount!!!")
    api.abortCalculation()

} else if (salesDiscount >= 0) {
    salesDiscount = salesDiscount / 100
    return salesDiscount

} else if (salesDiscount < 0) {
    api.criticalAlert("I won't let you bankrupt!!!")
    salesDiscount = salesDiscount / 100
    return salesDiscount
}

