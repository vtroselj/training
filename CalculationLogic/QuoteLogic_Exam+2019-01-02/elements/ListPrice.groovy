def listPrice = api.pricelist("Price List")

if (!listPrice) {
    api.redAlert("Missing List Price!!!")
    return null
} else {
    return listPrice
}

