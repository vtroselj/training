def invoicePrice

if (api.getElement("ListPrice") == null)  {
    api.redAlert("Missing List Price!!!")
    return null
} else if (api.getElement("SalesDiscount") == null) {
    api.redAlert("Missing Sales Discount!!!")
    return null
} else {
    invoicePrice = api.getElement("ListPrice") - api.getElement("SalesDiscount")
    return invoicePrice
}



