def cost = api.productExtension("Costs")?.find()?.attribute1

if (cost){
    return cost
} else {
    api.redAlert("Missing Cost!!!")
    return null
}


