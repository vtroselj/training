if(api.global.meat == null) { 
  return null
} else {
  def ce = api.createConfiguratorEntry()  

    api.global.sideDish.eachWithIndex { it, i ->
    	ce.createParameter(InputType.OPTION, "Side Dish_${i+1}").setValueOptions(api.global.sideDish)
        ce.createParameter(InputType.OPTION, "Side Dish Weight_${i+1}").setValueOptions("100g", "200g", "300g", "450g") 
        }
        ce.getInputs().each {
  			it.addParameterConfigEntry("noRefresh",true)
    	}
  	return ce
}
    

