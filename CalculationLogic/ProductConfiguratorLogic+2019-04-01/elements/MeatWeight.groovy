if(api.global.meat == null) { 
  return null
} else {
   def ce = api.createConfiguratorEntry(InputType.OPTION, "Meat Weight")
   ce.getFirstInput().setValueOptions("100g", "200g", "300g", "450g")
   ce.getFirstInput().addParameterConfigEntry("noRefresh",true)
   
   return ce
}