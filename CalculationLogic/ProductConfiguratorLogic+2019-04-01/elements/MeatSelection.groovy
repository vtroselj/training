if(api.isSyntaxCheck()) return

api.global.meat = api.findLookupTableValues("SideDish").name


def ce = api.createConfiguratorEntry(InputType.OPTION, "Meat")

ce.getFirstInput().setValueOptions(api.global.meat)
api.global.meat = api.input("Meat")

api.global.sideDish = api.findLookupTableValues("SideDish", Filter.equal("name", api.global.meat)).attribute1
api.global.sideDish = api.global.sideDish[0].split(", ")
//api.trace("","",api.global.sideDish)


return ce


