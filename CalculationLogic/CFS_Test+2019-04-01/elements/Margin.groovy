def margins = api.findLookupTableValues("MarginAdjustment")
def margin = api.findLookupTable("MarginAdjustment")
api.trace("Margins", "", margin)
def entry = [:]
if(margins) {
  for(m in margins) {
    a = [ lookupTableId: margin.id, lookupTableName: margin.uniqueName, name: m.name, attribute1: m.attribute1 + 0.01]
    api.trace("Margins", "", a)
    api.addOrUpdate("MLTV", a)
  }
}


